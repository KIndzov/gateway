package com.example.gateway.controllers;

import com.example.gateway.models.Entry;
import com.example.gateway.models.Ext_Service;
import com.example.gateway.models.dto.CurrencyOutput;
import com.example.gateway.models.dto.Mapper;
import com.example.gateway.models.dto.RequestDto;
import com.example.gateway.services.contracts.CurrencyService;
import com.example.gateway.services.contracts.LoggingService;
import com.sun.jdi.request.DuplicateRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/json_api")
public class JsonApiController {

    private final CurrencyService currencyService;
    private final LoggingService loggingService;
    private final Mapper mapper;

    @Autowired
    public JsonApiController(CurrencyService currencyService, LoggingService loggingService, Mapper mapper) {
        this.currencyService = currencyService;
        this.loggingService = loggingService;
        this.mapper = mapper;
    }

    @PostMapping("/current")
    public CurrencyOutput getCurrentData(@RequestBody RequestDto input) {
        try {
            loggingService.hasDuplicateId(input.getRequestId());
            CurrencyOutput output = mapper.dataToOutput(currencyService.getCurrentData(input.getCurrency()));
            Entry entry = new Entry(Ext_Service.EXT_SERVICE1_CURRENT.toString(), input.getRequestId(), LocalDateTime.now().toString(), input.getClient());
            loggingService.log(entry);
            return output;
        } catch (DuplicateRequestException e) {
            Entry entry = new Entry(Ext_Service.EXT_SERVICE_ERROR_DUPLICATE_REQUEST_ID.toString(), input.getRequestId(), LocalDateTime.now().toString(), input.getClient());
            loggingService.log(entry);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @PostMapping("history")
    public List<CurrencyOutput> getHistory(@RequestBody RequestDto input) {
        try {
            loggingService.hasDuplicateId(input.getRequestId());
            int hours = input.getPeriod();
            String base = input.getCurrency();
            List<CurrencyOutput> output = currencyService.getCurrencyHistory(base, hours).stream().map(mapper::dataToOutput).collect(Collectors.toList());
            Entry entry = new Entry(Ext_Service.EXT_SERVICE1_HISTORY.toString(), input.getRequestId(), LocalDateTime.now().toString(), input.getClient());
            loggingService.log(entry);
            return output;
        } catch (DuplicateRequestException e) {
            Entry entry = new Entry(Ext_Service.EXT_SERVICE_ERROR_DUPLICATE_REQUEST_ID.toString(), input.getRequestId(), LocalDateTime.now().toString(), input.getClient());
            loggingService.log(entry);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
