package com.example.gateway.controllers;

import com.example.gateway.models.Entry;
import com.example.gateway.models.Ext_Service;
import com.example.gateway.models.Rate;
import com.example.gateway.models.commandXml.Command;
import com.example.gateway.models.dto.CurrencyOutput;
import com.example.gateway.models.dto.Mapper;
import com.example.gateway.services.contracts.CurrencyService;
import com.example.gateway.services.contracts.LoggingService;
import com.sun.jdi.request.DuplicateRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/xml_api")
public class XmlApiController {

    private final CurrencyService currencyService;
    private final LoggingService loggingService;
    private final Mapper mapper;

    @Autowired
    public XmlApiController(CurrencyService currencyService, LoggingService loggingService, Mapper mapper) {
        this.currencyService = currencyService;
        this.loggingService = loggingService;
        this.mapper = mapper;
    }


    @PostMapping(value = "command", consumes = {MediaType.APPLICATION_XML_VALUE}, produces = {MediaType.APPLICATION_XML_VALUE})
    public List<CurrencyOutput> command(@RequestBody Command input) {
        if (input.getGet() != null && input.getHistory() == null) {
            return getCurrentData(input);
        }
        if (input.getHistory() != null && input.getGet() == null) {
            return getHistoryData(input);
        }

        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect input");
    }

    private List<CurrencyOutput> getCurrentData(Command input) {
        String client = input.getGet().getConsumer();
        String requestId = input.getId();
        String currency = input.getGet().getCurrency();
        List<CurrencyOutput> output = new ArrayList<>();
        try {
            loggingService.hasDuplicateId(requestId);
            List<Rate> rates = currencyService.getCurrentData(currency).getRatesList();
            CurrencyOutput currencyOutput = new CurrencyOutput(LocalDateTime.now().toString(),currency,rates);
            Entry entry = new Entry(Ext_Service.EXT_SERVICE2_CURRENT.toString(), requestId, LocalDateTime.now().toString(), client);
            loggingService.log(entry);
            output.add(currencyOutput);
            return output;

        } catch (DuplicateRequestException e) {
            Entry entry = new Entry(Ext_Service.EXT_SERVICE_ERROR_DUPLICATE_REQUEST_ID.toString(), requestId, LocalDateTime.now().toString(), client);
            loggingService.log(entry);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    private List<CurrencyOutput> getHistoryData(Command input){
        String client = input.getHistory().getConsumer();
        String requestId = input.getId();
        String currency = input.getHistory().getCurrency();
        int period = input.getHistory().getPeriod();
        List<CurrencyOutput> output;
        try{
            loggingService.hasDuplicateId(requestId);
            output = currencyService.getCurrencyHistory(currency,period).stream().map(mapper::dataToOutput).collect(Collectors.toList());
            Entry entry = new Entry(Ext_Service.EXT_SERVICE2_HISTORY.toString(), requestId, LocalDateTime.now().toString(), client);
            loggingService.log(entry);
            return output;
        } catch (DuplicateRequestException e) {
            Entry entry = new Entry(Ext_Service.EXT_SERVICE_ERROR_DUPLICATE_REQUEST_ID.toString(), requestId, LocalDateTime.now().toString(), client);
            loggingService.log(entry);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
