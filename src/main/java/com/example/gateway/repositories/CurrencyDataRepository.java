package com.example.gateway.repositories;

import com.example.gateway.models.CurrencyData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CurrencyDataRepository extends JpaRepository<CurrencyData,Long> {


    @Query("SELECT c FROM CurrencyData c WHERE c.date = (SELECT MAX(cc.date) FROM CurrencyData cc WHERE cc.base LIKE c.base)")
    List<CurrencyData> getLastData();

    @Query("SELECT c FROM CurrencyData c WHERE c.base LIKE ?1 AND c.date > ?2")
    List<CurrencyData> getHistoryData(String base, LocalDateTime dataAfter);
}
