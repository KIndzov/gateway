package com.example.gateway.repositories;

import com.example.gateway.models.Entry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface LogRepository extends JpaRepository<Entry,Long> {

    @Query("SELECT e FROM Entry e WHERE e.requestId LIKE ?1")
    List<Entry> getEntryByRequestId(String id);
}
