package com.example.gateway.models.dto;

public class RequestDto {

    private String requestId;
    private Long timestamp;
    private String client;
    private String currency;
    private int period;

    public RequestDto() {
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Long getTimeStamp() {
        return timestamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timestamp = timeStamp;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}
