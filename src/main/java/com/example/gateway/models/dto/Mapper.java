package com.example.gateway.models.dto;

import com.example.gateway.models.CurrencyData;
import com.example.gateway.models.Rate;
import org.springframework.stereotype.Component;

@Component
public class Mapper {

    public CurrencyOutput dataToOutput(CurrencyData currencyData) {
        CurrencyOutput currencyOutput = new CurrencyOutput();
        currencyOutput.setBaseCurrency(currencyData.getBase());
        currencyOutput.setTimeStamp(currencyData.getDate().toString());
        for (Rate rate : currencyData.getRatesList()) {
            currencyOutput.getRatesList().add(rate);
        }


        return currencyOutput;
    }
}
