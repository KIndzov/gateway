package com.example.gateway.models.dto;

import com.example.gateway.models.Rate;

import java.util.ArrayList;
import java.util.List;

public class CurrencyOutput {
    private String timeStamp;
    private String baseCurrency;
    private List<Rate> ratesList = new ArrayList<>();

    public CurrencyOutput() {
    }

    public CurrencyOutput(String timeStamp, String baseCurrency, List<Rate> ratesList) {
        this.timeStamp = timeStamp;
        this.baseCurrency = baseCurrency;
        this.ratesList = ratesList;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public List<Rate> getRatesList() {
        return ratesList;
    }

    public void setRatesList(List<Rate> ratesList) {
        this.ratesList = ratesList;
    }
}
