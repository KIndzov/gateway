package com.example.gateway.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table
public class Rate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;
    @Column
    private String currency;
    @Column
    private Double rate;
    @JsonIgnore
    @ManyToOne
    private CurrencyData currencyData;

    public Rate() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public CurrencyData getCurrencyData() {
        return currencyData;
    }

    public void setCurrencyData(CurrencyData currencyData) {
        this.currencyData = currencyData;
    }
}
