package com.example.gateway.models;

import javax.persistence.*;

@Entity
@Table
public class Entry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String serviceName;
    @Column
    private String requestId;
    @Column
    private String timestamp;
    @Column
    private String endClient;

    public Entry() {
    }

    public Entry(String serviceName, String requestId, String timestamp, String endClient) {
        this.serviceName = serviceName;
        this.requestId = requestId;
        this.timestamp = timestamp;
        this.endClient = endClient;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getEndClient() {
        return endClient;
    }

    public void setEndClient(String endClient) {
        this.endClient = endClient;
    }
}
