package com.example.gateway.models.commandXml;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Command {
    private String id;
    private Get get;
    private History history;

    public Command() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Get getGet() {
        return get;
    }

    public void setGet(Get get) {
        this.get = get;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }
}
