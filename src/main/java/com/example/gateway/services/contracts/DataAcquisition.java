package com.example.gateway.services.contracts;

import com.example.gateway.models.CurrencyData;

import javax.management.ServiceNotFoundException;
import java.util.Map;

public interface DataAcquisition {

    public Map<String,CurrencyData> getCurrencyData() throws ServiceNotFoundException;

    public Map<String,CurrencyData> getLastData();
}
