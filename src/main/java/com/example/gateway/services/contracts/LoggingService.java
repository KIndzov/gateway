package com.example.gateway.services.contracts;

import com.example.gateway.models.Entry;

public interface LoggingService {

    public void log(Entry entry);

    public boolean hasDuplicateId(String id);
}
