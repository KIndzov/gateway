package com.example.gateway.services.contracts;

import com.example.gateway.models.CurrencyData;

import javax.management.ServiceNotFoundException;
import java.util.List;

public interface CurrencyService {

    CurrencyData getCurrentData(String base);

    List<CurrencyData> getCurrencyHistory(String base, int hours);

    void updateCurrentData() throws ServiceNotFoundException;




}
