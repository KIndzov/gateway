package com.example.gateway.services;

import com.example.gateway.services.contracts.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.management.ServiceNotFoundException;

@Service
@PropertySource("scheduled.properties")
public class ScheduledService {

    private static final long THREE_SECONDS = 3 * 1000;
    private static final long ONE_MINUTE = 60 * 1000;
    private static final long TEN_MINUTE = 10 * 60 * 1000;
    private final CurrencyService currencyService;

    @Autowired
    public ScheduledService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @Scheduled(fixedRateString = "#{${scheduled.rate.minutes}*60*1000}")
    public void getData() throws ServiceNotFoundException {
        currencyService.updateCurrentData();
    }
}
