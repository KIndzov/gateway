package com.example.gateway.services;

import com.example.gateway.models.Entry;
import com.example.gateway.repositories.LogRepository;
import com.example.gateway.services.contracts.LoggingService;
import com.sun.jdi.request.DuplicateRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoggingServiceImp implements LoggingService {
    private final LogRepository logRepository;

    @Autowired
    public LoggingServiceImp(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    @Override
    public void log(Entry entry) {
              logRepository.save(entry);
    }

    @Override
    public boolean hasDuplicateId(String id) {
        if(logRepository.getEntryByRequestId(id).isEmpty()){
            return true;
        }
        throw new DuplicateRequestException("Duplicate requestId");
    }
}
