package com.example.gateway.services;

import com.example.gateway.models.CurrencyData;
import com.example.gateway.repositories.CurrencyDataRepository;
import com.example.gateway.services.contracts.CurrencyService;
import com.example.gateway.services.contracts.DataAcquisition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.ServiceNotFoundException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    private final CurrencyDataRepository currencyDataRepo;
    private final DataAcquisition dataAcquisition;

    private Map<String, CurrencyData> currentData = new HashMap<>();

    @Autowired
    public CurrencyServiceImpl(CurrencyDataRepository currencyDataRepo, DataAcquisition dataAcquisition) {
        this.currencyDataRepo = currencyDataRepo;
        this.dataAcquisition = dataAcquisition;
    }

    @Override
    public CurrencyData getCurrentData(String base) {
        if (currentData != null && currentData.containsKey(base)){
            return currentData.get(base);
        }
        try{
            return dataAcquisition.getCurrencyData().get(base);
        } catch (ServiceNotFoundException e){
            currentData = dataAcquisition.getLastData();
            return currentData.get(base);
        }
    }

    @Override
    public List<CurrencyData> getCurrencyHistory(String base, int hours) {
        LocalDateTime dataAfter = LocalDateTime.now().minusHours(hours);

            return currencyDataRepo.getHistoryData(base, dataAfter);

    }

    @Override
    public void updateCurrentData() throws ServiceNotFoundException {
        currentData = dataAcquisition.getCurrencyData();
    }


}
