package com.example.gateway.services;

import com.example.gateway.models.CurrencyData;
import com.example.gateway.models.Rate;
import com.example.gateway.repositories.CurrencyDataRepository;
import com.example.gateway.services.contracts.DataAcquisition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.management.ServiceNotFoundException;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class DataAcquisitionImpl implements DataAcquisition {

    private final CurrencyDataRepository currencyDataRepository;


    @Autowired
    public DataAcquisitionImpl(CurrencyDataRepository currencyDataRepository) {
        this.currencyDataRepository = currencyDataRepository;

    }

    @Override
    public Map<String,CurrencyData> getCurrencyData() throws ServiceNotFoundException {
        final String uri = "http://data.fixer.io/api/latest?access_key=d51da8e6fac0570cd54a7571d8e7be04";
        RestTemplate restTemplate = new RestTemplate();
        CurrencyData currencyData = new CurrencyData();
        Map<String,CurrencyData> currentData = new HashMap<>();

        try {
            var result = restTemplate.getForObject(uri, Map.class);
            currencyData.setBase((String) result.get("base"));
            currencyData.setDate(LocalDateTime.now());
            Map<String, Object> rates = (Map<String, Object>) result.get("rates");
            for (String s : rates.keySet()) {
                Rate rate = new Rate();
                rate.setCurrency(s);
                rate.setRate(Double.valueOf(rates.get(s).toString()));
                rate.setCurrencyData(currencyData);
                currencyData.getRatesList().add(rate);
            }
            currencyDataRepository.save(currencyData);
            currentData.put(currencyData.getBase(),currencyData);
        } catch (Exception e){
            throw new ServiceNotFoundException();
        }

        return currentData;
    }

    @Override
    public Map<String, CurrencyData> getLastData() {
        Map<String,CurrencyData> currentData = new HashMap<>();
        List<CurrencyData> currencyDataList = currencyDataRepository.getLastData();
        for (CurrencyData currencyData : currencyDataList) {
            currentData.put(currencyData.getBase(),currencyData);
        }

        return currentData;
    }
}
